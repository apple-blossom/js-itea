/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/


var Train = {
	name: "Kiev-Odessa",
	speed: "80kmh",
	passengers: 0,
	go : function () {
		alert(`Train ${this.name} is going ${this.speed} with ${this.passengers} on board`);
	},

	stop: function () {
		alert(`Train ${this.name} stopped. Speed is 0`);
	},

	addPass: function (passNumber) {
		this.passengers += passNumber;
	}
};

Train.addPass(2);
Train.go();
Train.stop();


