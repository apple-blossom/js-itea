/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/



var alphabet = "abcdefghijklmnopqrstuvwxyz";

function cypher (num, word) {
  let arrWord = word.toLowerCase().split('');
  for (var i = 0; i <= arrWord.length - 1; i++) {
      console.log(arrWord[i]);
    var numInAlph = alphabet.indexOf(arrWord[i]) + num;
    if (numInAlph >= alphabet.length - 1) {
      numInAlph = numInAlph % alphabet.length;
    } 
    arrWord[i] = alphabet[numInAlph];
    console.log(arrWord[i], numInAlph);
  }
  var result = arrWord.join('');
  console.log(result);
  return result;
}

cypher(2, "de")
cypher(2, "zabc")
cypher(5, "WXYZABC")

function decypher (num, word) {
  let arrWord = word.toLowerCase().split('');
  for (var i = 0; i <= arrWord.length - 1; i++) {
      console.log(arrWord[i]);
    var numInAlph = alphabet.indexOf(arrWord[i]) - num;
    if (numInAlph < 0) {
      numInAlph = alphabet.length - Math.abs(numInAlph);
    } 
    arrWord[i] = alphabet[numInAlph];
    console.log(arrWord[i], numInAlph);
  }
  var result = arrWord.join('');
  console.log(result);
  return result;
}

decypher(2, "bcde")
decypher(2, "fg")

function cypherDecypher(encrypt, decrypt) {
	return "Result:" + encrypt + " " + decrypt; 
}

curry = cypherDecypher.bind(null, decypher(3, "AZxyB"));
curry();


