/*

  Задание:


    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }

      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>
*/
'use strict';

var CommentProto = {
	avatarUrl: 'https://help.salesforce.com/resource/1565392064000/HelpStaticResource/assets/images/tdxDefaultUserLogo.png',
	addLikes () {
		this.likes++;
		console.log(this.likes);
		return this.likes;
	}
}


function Comment(name, txt, avatarUrl, likes = 0) {
    this.name = name;
    this.txt = txt;
	this.likes = likes;
	this.__proto__ = CommentProto;
	if (avatarUrl!= null) {
		this.avatarUrl = avatarUrl;
	}
}


var myComment1 = new Comment("User1", "This is test");
myComment1.avatarUrl;
myComment1.addLikes();

var myComment2 = new Comment("User2", "This is test 2");
myComment2.addLikes();
var myComment3 = new Comment("User3", "This is test 3", "https://pbs.twimg.com/profile_images/685347483430486016/tr4jiGUK_400x400.jpg");
var myComment4 = new Comment("User4", "This is test 4", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi6ViUDmeM2Fn8HNQGa4dIujdkWzESGUbA-YYPRhtg0f0coPaHHQ");


 var CommentsArray = [myComment1, myComment2, myComment3, myComment4]



function ShowComments(comments) {
    for(var i = 0; i < comments.length; i++) {
		var div = document.createElement('div');
		var p1 = document.createElement('p');
		p1.innerHTML = comments[i].name;
		var p2 = document.createElement('p');
		p2.innerHTML = comments[i].txt;
		var p3 = document.createElement('p');
		p3.innerHTML = comments[i].likes;
		var img = document.createElement('img');
		img.src = comments[i].avatarUrl;
		div.appendChild(p1);
		div.appendChild(p2);
		div.appendChild(p3);
		div.appendChild(img);
		document.body.appendChild(div)
	}
}

ShowComments(CommentsArray);





