/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
	
function getRandHex() {
	var color = getRandomIntInclusive(0, 255);
	color = color.toString(16);
	return color;
}

function randomHexRgb() {
	var r = getRandHex();
	var g = getRandHex();
	var b = getRandHex();
	return "#" + r + g + b;
}

function changeColor(selector, sel2) {
	var c = randomHexRgb();
	document.querySelector(selector).style.background = c;
	localStorage.setItem('bg', c);
}

window.onload = function() {
	console.log(localStorage.getItem('bg'))
	if (localStorage.getItem('bg') != null) {
		document.body.style.background = localStorage.getItem('bg');
	}
  var but = document.createElement("button");
  but.style = "display: block; margin: auto; height: 50px; width: 200px;"
  document.body.appendChild(but);
  but.addEventListener('click', () => {changeColor("body", ".color")})
  but.innerHTML = "Change color!";

};