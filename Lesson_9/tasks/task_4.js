/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */


class Birdy {
  constructor(name) { 
    this.name = name;
    this.nest = true;
    this.fly = true;
    this.swim = false;
    this.eat = true;
    this.hunt = true;
    this.sing = true;
    this.post = false;
    this.run = false;
  }
  running() {
      var div = document.createElement('div');
      document.body.appendChild(div);
      if (this.run == true) {
      for(var i=0; i<=5; i++){
        div.innerHTML +=  `/\\`;
      }
      div.innerHTML +=  `🐤 pribezhal!`
      } else {
        div.innerHTML +=  `ne begaet! :(`;
      }   
    };
  singing() {
      var div = document.createElement('div');
      document.body.appendChild(div);
      if (this.sing == true) {
      for(var i=0; i<=5; i++){
        div.innerHTML +=  `chirik!`;
      }
      } else {
        div.innerHTML +=  `ne poyot! :(`;
      }   
    };
  flying() {
      var div = document.createElement('div');
      document.body.appendChild(div);
      if (this.fly == true) {
        for(var i=0; i<=5; i++){
          div.innerHTML +=  `--------🕊`;
        }
      } else {
        div.innerHTML +=  `ne letit! :(`;
      }   
    };
  }


let Owl = new Birdy("Owly");
Owl.post = true;
Owl.posting = () => {
  var div = document.createElement('div');
  document.body.appendChild(div);
  div.innerHTML +=  `-------`;
  div.innerHTML +=  `💌 vam pis'mo!`;
}
Owl.flying();
Owl.posting();

let Nightingale = new Birdy("Nightingale");
Nightingale.singing();
Nightingale.running();

let Chicken = new Birdy("Owly");
Chicken.run = true;
Chicken.running();
Chicken.singing();

