/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/

class Posts {
  constructor(about, _id, created_at, isActive, title) { 
    this._id = _id;
    this.isActive = isActive;
    this.title = title;
    this.about = about;
    this.likes = 0;
    this.created_at = created_at;
  }
    render() {
      var div = document.createElement('div');
      div.innerHTML +=  `<div>`;
      div.innerHTML +=   `<h3> Title: ${this.title}</h3>`;
      div.innerHTML +=   `<p> About: ${this.about} </p>`;
      div.innerHTML +=   `<p> Created: ${this.created_at} </p>`;
      div.innerHTML +=   `<p> Is Active? : ${this.isActive} </p>`;
      div.innerHTML +=   `<button class='like'> Likes: ${this.likes}</button>`;
      div.innerHTML +=  `</div>`;
      //console.log(document.querySelector("button"));
      div.querySelector(".like").addEventListener('click', (event) => {
        //console.log(event.target);
          this.addLike();
          //event.target.innerHTML = ""; 
          event.target.innerHTML=  `Likes: ${this.likes}`;
          console.log(this.likes); 
        }
        )
      document.body.appendChild(div);
    };

    addLike() {
      this.likes += 1;
    }
}

const convertToJson = res => res.json(); 

 showAll = (objectJson) => {
   for (variable in objectJson) {
    ob = objectJson[variable];
    el = new Posts(ob.about, ob._id, ob.created_at, ob.isActive, ob.title);
    el.render();
  }
}

fetch("http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2")
    .then( convertToJson ).then(showAll)
