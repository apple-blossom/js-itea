/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678


*/

window.onload = () => {
	var frm = document.querySelector("#form1");
	var hello = document.createElement('h1');
	var logout = document.createElement('button');

	showHello = () => {
		frm.classList.add("hidden");
		hello.innerText = "Hello, " + localStorage.getItem('login') + "!";
		document.body.appendChild(hello);
		logout.innerText = "Log Out";
		document.body.appendChild(logout);
		logout.classList.remove("hidden");
		hello.classList.remove("hidden");
		logout.addEventListener('click', () => {
			localStorage.clear();
			frm.classList.remove("hidden");
			logout.classList.add("hidden");
			hello.classList.add("hidden");
		})
	}

	if (localStorage.getItem('login') == null && localStorage.getItem('pass') == null ) {
		frm.classList.remove("hidden");
	} else {
		showHello();
	}
  var frm = document.querySelector("#form1");
  frm.addEventListener('submit', (event) => {
  		event.preventDefault();
  		localStorage.setItem('login', event.target.name.value);
  		localStorage.setItem('pass', event.target.password.value);
  		showHello();
  })


};