/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адрес |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

async function getData(){

const listResp = await fetch("http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2");
const users = await listResp.json();

console.log(users);

 document.body.innerHTML = "# | Company  | Balance | Показать дату регистрации | Показать адресс |";

 users.forEach((item) => {
 
    console.log(item);
	var div = document.createElement('div');
    div.innerHTML += `<br>`;
    div.innerHTML += users.indexOf(item) + 1;
    div.innerHTML += "   |   ";
    div.innerHTML += item.company;
    div.innerHTML += "   |   ";
    div.innerHTML += item.balance;
    div.innerHTML += "   |   ";
    div.innerHTML += `<button class="regDate">Показать дату регистрации</button>`;
    div.innerHTML += "   |   ";
    div.innerHTML += `<button class="address">Показать адрес</button>`;
	
	div.querySelector('.regDate').addEventListener(
	'click', () => {
		event.target.classList.add('hidden');
		var div = document.createElement('span');
		div.innerHTML += item.registered;
		event.target.parentNode.insertBefore(div, event.target);
	}
	)
	div.querySelector('.address').addEventListener(
	'click', () => {
		showItems(event.target, item.address)
	}
	)
		document.body.appendChild(div);
 }

 )//showItems(item.address)

}

let showItems = (target, obj) => {
	event.target.classList.add('hidden');
	var span = document.createElement('span');
	for (item in obj) {
		console.log("ierm" + item);
		span.innerHTML  +=  ` <br>` + item + ":" + obj[item]
		target.parentNode.insertBefore(span, event.target);
	}
}


  var dat = getData();
        dat.then( data => console.log('res', data));

