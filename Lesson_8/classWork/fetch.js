/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/


function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

var opt1 = {
  method: 'GET'
}

  showAll = (objectJson) => {
	 for (variable in objectJson) {
		let p = document.createElement('p');
		p.innerText = `${variable} : ${objectJson[variable]}`;
		document.body.appendChild(p);
	}
 
 };
 
 parseObj = (obj) => {
	return obj.map(element => element.name)
 }

const convertToJson = res => res.json(); 

  fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2", opt1)
    .then( convertToJson )
    .then( response1 => response1[getRandomInt(response1.length - 1)])
    .then( res => {
       return fetch("http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2", opt1)
        .then( convertToJson )
        .then( respObj => {
          return Object.assign({
            name: res.name    
        },
		parseObj(respObj[0].friends))
    })
    })
    .then( showAll )