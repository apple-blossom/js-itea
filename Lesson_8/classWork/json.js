
/*
  Задание:
  Написать скрипт, который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут, который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Array

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/
let form = document.querySelector("#form1");
let form2 = document.querySelector("#form2");

form.addEventListener("submit", (event) => {
event.preventDefault();
const formData = Array.from(event.target);
formDP = formData.reduce(function(acc, cur, i) {
  acc[cur.name]= cur.value;
  return acc;
}, {});
formDP = JSON.stringify(formDP); 
let obj = JSON.parse(formDP);
console.log(obj);
}
)

form2.addEventListener("submit", (event) => {
event.preventDefault();
const formData = Array.from(event.target);
formDP = formData.reduce(function(acc, cur, i) {
  acc[cur.name]= cur.value;
  return acc;
}, {});
formDP = JSON.stringify(formDP); 
let obj = JSON.parse(formDP);
console.log(obj);
}
)



