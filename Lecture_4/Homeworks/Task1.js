

    /*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попал

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        
        Задание:
        Используя браузерное API для валидации форм реализовать валидацию следующей формы:

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */

    function generateInput () {
        return document.createElement('input');
    }

    function generateLabel () {
        return document.createElement('label');
    }

    function generateError (el, err) {
        var msg = document.createElement('div');
        msg.innerHTML = err;
        el.appendChild(err);
    }

    var form = document.createElement('form');
    form.style.display = 'flex';
    form.style.flexDirection = 'column';
    form.style.width = '40%';
    document.body.appendChild(form)

    function genNewInput (lblText, type, elClass) {
        var lbl = generateLabel();
        lbl.innerHTML = lblText;
        form.appendChild(lbl);
        var el = generateInput();
        el.type = type;
		el.classList.add(elClass);
		el.setAttribute("required","");
		//el.setCustomValidity(errMsg);
		el.reportValidity();
        lbl.appendChild(el);
		return document.querySelector("." + elClass)
    }
	
	var usrName = genNewInput('Имя пользователя: ', 'text', 'usrName');
	usrName.setAttribute("minlength", "2");
	usrName.addEventListener("input", function (event) {
		  if (usrName.validity.valueMissing) {
			usrName.setCustomValidity("Как тебя зовут, дружище?!");
		  } else {
			usrName.setCustomValidity("");
		  }
		});	
	var usrEmail = genNewInput('Почта: ', 'email', 'usrEmail');
	usrEmail.setAttribute("minlength", "3");
	usrEmail.setAttribute("validEmail", "true");
	usrEmail.addEventListener("input", function (event) {
		  if (usrEmail.validity.typeMismatch) {
			usrEmail.setCustomValidity( "Ну и зря, не получишь бандероль с яблоками!");
		  } else {
			usrEmail.setCustomValidity("");
		  }
		});	
	var usrPass = genNewInput('Пароль: ', 'password', 'usrPass');
	usrPass.setAttribute("minlength", "8");
	usrPass.setAttribute("maxlength", "16");
		usrPass.addEventListener("input", function (event) {
			if (usrPass.validity.valueMissing) {
			usrPass.setCustomValidity("Я никому не скажу наш секрет");
		  } else {
			usrPass.setCustomValidity("");
		  }
		});	
	var appelsNum = genNewInput('Количество сьеденых яблок: ', 'number', 'appelsNum');
	appelsNum.setAttribute("minlength", "1");
	appelsNum.setAttribute("validNumber", "true");
	appelsNum.addEventListener("input", function (event) {
	if (usrPass.value == "0") {
			usrPass.setCustomValidity("Ну хоть покушай немного... Яблочки вкусные");
		  } else {
			usrPass.setCustomValidity("");
		  }
		});	
	var thnx = genNewInput('Напиши спасибо за яблоки: ', 'text', 'thnx');
    thnx.addEventListener("input", function (event) {
		if (usrPass.validity.valueMissing) {
			usrPass.setCustomValidity("Фу, неблагодарный(-ая)!");
		  } else {
			usrPass.setCustomValidity("");
		  }
		});	
	var learning = genNewInput('Согласен на обучение: ', 'checkbox', 'learning');
	learning.addEventListener("input", function (event) {
	if (usrPass.validity.valueMissing) {
			usrPass.setCustomValidity("Необразованные живут дольше! Хорошо подумай!");
		  } else {
			usrPass.setCustomValidity("");
		  }
		});	

    var sbmt = document.createElement('input');
	sbmt.type = "submit";
	form.appendChild(sbmt);
    var val = document.createElement('button');
	val.innerHTML = "Проверка";
	form.appendChild(val);

	document.querySelector('button').addEventListener('click', function() {
		var msg = document.createElement('div');
		msg.innerHTML = "";
		if (!form.checkValidity()) {
        msg.innerHTML = "Неверно!!";
		} else {
		msg.innerHTML = "Верно!!";
		}
		form.appendChild(msg);
}
);


