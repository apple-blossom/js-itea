window.addEventListener("load", () => {
    

class DatePicker {
	constructor(selector){
		this.selector = document.querySelector(selector);
		this.selectedDate = {};
		this.curTime = new Date();
		this.months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
		this.selector.addEventListener('submit', (event) => {
			event.preventDefault();
			console.log(this.selectedDate);
			this.selectedDate.date = this.selector.querySelector("input._superDate[name=date]").value;
			this.selectedDate.time = this.selector.querySelector("input._superDate[name=time]").value;
			this.selectedDate.dateTo = this.selector.querySelector("input._superDate[name=dateTo]").value;
			this.selectedDate.timeTo = this.selector.querySelector("input._superDate[name=timeTo]").value;
			localStorage.setItem('pickedDate', JSON.stringify(this.selectedDate));
			alert("Dates saved!");
			  }

		);

		if (localStorage.getItem('pickedDate') != null) {
			let parsed = JSON.parse(localStorage.getItem('pickedDate'));
			this.selector.querySelector("input._superDate[name=time]").value = parsed.time;
			this.selector.querySelector("input._superDate[name=date]").value = parsed.date;
			this.selector.querySelector("input._superDate[name=timeTo]").value = parsed.timeTo;
			this.selector.querySelector("input._superDate[name=dateTo]").value = parsed.dateTo;

		}

		this.selector.querySelector(".dt-calendar__controls").querySelector(".timeCh").addEventListener('change', (event) => {
			if (event.target.checked) {
				this.selector.querySelector("input._superDate[name=time]").classList.remove("dt-hidden");
			} else {
				this.selector.querySelector("input._superDate[name=time]").classList.add("dt-hidden");
			}
		}
		);

		this.selector.querySelector(".dateToCh").addEventListener('change', (event) => {
			this.applDateAndTimeTo();
			if (event.target.checked) {
				this.selector.querySelector(".dt-datesTo").classList.remove("dt-hidden");
			} else {
				this.selector.querySelector(".dt-datesTo").classList.add("dt-hidden");
			}
		}
		);

	}

	// showDatesTo = () => {
	// 	this.selector.querySelector(".dateToCh").addEventListener('change', (event) => {
	// 		if (event.target.checked) {
	// 			this.selector.querySelector(".dt-datesTo").classList.remove("dt-hidden");
	// 		} else {
	// 			this.selector.querySelector(".dt-datesTo").classList.add("dt-hidden");
	// 		}
	// 	}
	// 	);
	// }

	formatDate = (date) => {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) 
	        month = '0' + month;
	    if (day.length < 2) 
	        day = '0' + day;

	    return [year, month, day].join('-');
	}
	
	showDateInput = (month, year, e, name = "From") => {
		let nameSelector = ".dt-dates__block" + name;
		if (document.querySelectorAll(nameSelector).length == 0) {
			let result = '';
			let	newDiv = document.createElement("div");
			result += '<div class="dt-wrapper">';
			result += '<div class="dt-header--wrap">';
			result += '<span class="dt-header">Select date</span>';
			result += '<span class="dt-close">✖</span>';
			result += '</div>';
			result += '<form class="dt-ym-form">';
			result += '<select name="month" class="dt-month" >';
			for(let index in this.months){
			  if( index == month )
				result += `<option value="${index}" selected>${this.months[ index ]}</option>`;
			  else
				result += `<option value="${index}">${this.months[ index ]}</option>`;
			}
			result += '</select>';
			result += '<input name="year" type="number" class="dt-year" value="' + year + '"/>';
			result += '</form>';
			result += '<div class="dt-dates">';
			result += '</div>';
			result += '</div>';
			result += '';
			newDiv.innerHTML = result;
			newDiv.classList.add("dt-dates__block" + name);
			document.body.appendChild(newDiv);
			newDiv.querySelector(".dt-dates").innerHTML = this.generateDates(month, year);		
			newDiv.querySelector(".dt-ym-form").addEventListener('change', (event) => {
					newDiv.querySelector(".dt-dates").innerHTML = this.generateDates(event.target.parentNode.month.value, event.target.parentNode.year.value);
					newDiv.querySelectorAll(".dt-dates span").forEach((item) => {
						item.addEventListener('click', (event) => {
							newDiv.querySelector(".dt-dates .dt-picked").classList.remove('dt-picked');
							event.target.classList.add('dt-picked');
							let date = new Date(event.target.dataset.year, event.target.dataset.month, event.target.innerText);
							let resDate = this.formatDate(date);
							e.target.value = resDate;
						}
						)
					}
					);
			  }
			);
			
			newDiv.querySelector(nameSelector+" .dt-close").addEventListener('click', (event) => {
					document.querySelector(nameSelector).classList.add("dt-hidden");
					e.target.removeAttribute("disabled", "");
			  }
			);

			newDiv.querySelectorAll(".dt-dates span").forEach((item) => {
						item.addEventListener('click', (event) => {
						let date = new Date(event.target.dataset.year, event.target.dataset.month, event.target.innerText);
						if (newDiv.querySelectorAll(".dt-dates .dt-picked").length != 0){newDiv.querySelector(".dt-dates .dt-picked").classList.remove('dt-picked');}
						event.target.classList.add('dt-picked');
						let resDate = this.formatDate(date);
						e.target.value = resDate;
						}
						)
					}
					);
		} else {
			document.querySelector(nameSelector).classList.remove("dt-hidden");

		}
		
	}
	///
	generateDates = (month, year) => {
    let result = '';
    let counter = 0;
    let curDate = new Date(year, month, 1, 0, 0, 0, 0);    
    let startDatOffset = curDate.getDay() * -1 + 2;
    
    for(let week=0; week<6; week++) {
      result += '<div class="dt-row">';
      
      for(let day=0; day<7; day++) {
        
        let d = new Date(year, month, counter + startDatOffset, 0, 0, 0, 0);        
        if( d.getMonth() == month )
          result += `<span class="dt-curmonth" data-month="${d.getMonth()}" data-year="${d.getFullYear()}" >${d.getDate()}</span>`;
        else 
          result += `<span class="dt-not-curmonth" data-month="${d.getMonth()}" data-year="${d.getFullYear()}" >${d.getDate()}</span>`;
        
        counter ++;
      }
      
      result += '</div>';
    }
    
    return result;
  }

  showTime = (e, name = "From") => {
  	let nameSelector = ".dt-time__block" + name;
  	if (document.querySelectorAll(nameSelector).length == 0) {
		  	let result = '';
		  	let	newDiv = document.createElement("div");
			newDiv.classList.add("dt-time__block" + name);
			document.body.appendChild(newDiv);
			result += '<div class="dt-header--wrap">';
			result += '<span class="dt-header">Select time</span>';
			result += '<span class="dt-close">✖</span>';
			result += '</div>';
		  	result += '<div class="dt-time--wrap">';
			result += `<input type="time" class="dt-time__clock"></input>`;
			result += '</div>';

		  	newDiv.innerHTML = result;
			newDiv.querySelector(nameSelector + " .dt-close").addEventListener('click', (event) => {
							document.querySelector(nameSelector).classList.add("dt-hidden");
							e.target.removeAttribute("disabled", "");
					  }
					);

			newDiv.querySelector(".dt-time__clock").addEventListener('change', (event) => {
							e.target.value = event.target.value;
							console.log(this.selectedDate);
					  }
					);
	} else {
			document.querySelector("nameSelector").classList.remove("dt-hidden");
		}

  }
  
  applDate = () => {
	this.selector.querySelector("input._superDate[type=date]").addEventListener('click', (event) => {
				event.preventDefault();
				this.showDateInput(this.curTime.getMonth(), this.curTime.getFullYear(), event);
				event.target.setAttribute("disabled", "");
		  }
		);

   }

   applTime = () => {
	this.selector.querySelector("input._superDate[name=time]").addEventListener('click', (event) => {
				event.preventDefault();
				this.showTime(event);
				event.target.setAttribute("disabled", "");
		  }
		);
   }

   applDateAndTimeTo = () => {
   	this.selector.querySelector("input._superDate[name=dateTo]").addEventListener('click', (event) => {
				event.preventDefault();
				this.showDateInput(this.curTime.getMonth(), this.curTime.getFullYear(), event, "To");
				event.target.setAttribute("disabled", "");
		  }
		);
   	this.selector.querySelector("input._superDate[name=timeTo]").addEventListener('click', (event) => {
				event.preventDefault();
				this.showTime(event, "To");
				event.target.setAttribute("disabled", "");
		  }
		);


   }
	
}

let tst = new DatePicker("#datePickerWrapper");
tst.applDate();
tst.applTime();








	
	
});